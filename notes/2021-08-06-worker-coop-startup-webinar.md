
Worker Co-op Startup Webinar

Notes 2021-08-06

Luke and Sam attending



Lead by Matt Feinstein



   * Definitional info on woorker coops vs other forms of coops. 400-700 they (USFWC) identify, A diverse breakdown, majority non-white.
   * Reviews ICC principles, highlighting some points
       * Non-exclusionary
       * One member one vote (not shares)
       * Training is ongoing process for democratic operation
       * Coops support each other
   * Aims of worker coops:
       * Democracy: sharing decisions
       * Joint prosperity: sharing wealth
       * Shared vision of community
   * [Focus on startups here, but messier possibility of conversion of existing business to coop]


Shift to starting up: where are you strong and where do you need help?



   * Coop entrepreneur-ing: 3 Areas
       *  **Co-op capacity development**
           * Individual vision to shared vision - membership, accountability
           * Form a learning group, meet regularly with an agenda
           * What is our common economic or social need?
           * Examples from independent film production pooling resources, marketing; worker-owned farm forming out of worker center with focus on food and worker rights.
           * Clarify who is a member, what the rights and responsibilities of members will be.
               * In realms incl Financial Participation, Education, Governance, Information
           * Learn to delegate work!
               * Find real tasks for everone! Expect it to be hard!
               * Practice accountability: clear tasks, in meeting minutes, checking in on how and why, celebrate accomplishments.
               * It is important to set good practices - task lists, accountability, peer check ins, grievance procedures - from the start.
       * **Business development**
           * Make sure you are running a business that will continue
           * Business form, capital.
           * Making sure you are creating value for someone - what customers need to solve what problem with what product or service? Value proposition. Business model canvas (tool)
           * Create a legal entity
               * How do people become members, how do we make decisions, is there a cost to join, how will profit be divided, how to handle outside investors.
               * Often a high-bar for membership due to voting power.
               * Profit usually divided by hours worked over the year
               * Talk to a lawyer for state info, with your answers to above. Corp or LLC may be fine.
                   * Issues of owners vs employees, esp.
                   * Side point: IRS subchapter T criteria allowing reinvestment back to member individuals as patronage 
                       * [https://www.law.cornell.edu/uscode/text/26/subtitle-A/chapter-1/subchapter-T](https://www.law.cornell.edu/uscode/text/26/subtitle-A/chapter-1/subchapter-T)
           * Raise startup capital
               * Coop-specific lenders are out there
                   * [https://sharedcapital.coop/](https://sharedcapital.coop/)
                   * [https://seedcommons.org/](https://seedcommons.org/)
                   * [https://www.theworkingworld.org/us/](https://www.theworkingworld.org/us/)
               * Don't let one member charge it all on their card
               * Account for non-voting investor equity, talk to lawyer
           * Governance and management structures
               * Board? that hires manager/team
               * Or Committee / sociocracy structures
               * Collective/horizontal. Creativity is fine, transparency/clarity is key
       * **Ecosystem development** - customers, other coops
           * Where can we get technical assistance?
               * Coop developer resources - democratic process, structure questions
               * Also small business info in general, marketing, financial projecting, etc
           * How to connect to social movements?
               * Trying to be a catalyst for social change?
               * Accountable to living up to your promises to community
               * Member councils in USFWC on justice, advocacy, unions
           * Get commitment and input from customers
               * Presales, community events, engagement
               * Local anchor institutions as source of initial contracts
               * Align with government goals - preferential vendors, workforce development initiatives, geographic
   * How are you building/reaching out for your team development?
   * Join USFWC as a "Startup" member
       * Startup consultation
       * Health benefits
       * Events and council groups


[http://co-oplaw.org/](http://co-oplaw.org/)

[https://usworker.coop/resources/](https://usworker.coop/resources/)



Q\&A



   * How big to start? 5-7, 3-15
   * How soon do you recommend a legal entity?
       * At least 3 members
       * Before purchasing assets
       * Processes and governance initial decisions
       * Revenue projection in place
   * Thoughts on decision process, consensus etc
       * thresholds for key decisions: dissolution, bylaws changes
   * Thoughts on process of writing bylaws
       * Bylaws governance - how are we organized - separate from operations decision making
           * note from the peanut gallery: at other co-ops I've found it very useful to have separate policy documents for day-to-day operations and have examples that I can provide if we talk about this later. Separation of the board and a general manager who handles operations has also been helpful.
           * ditto to that ^ the farmers market I ran did a nice job of splitting bylaws and policies, and had a day-to-day manager role with non-governance power.
   * Plan for conflict resolution
   * Building beyond lone vision: Look for outside facilitation, participatory retreat/academy learning together with team
       * LA Coop academy offers an online one.
   * How to get beyond being one person interested?
       * Consider "interviewing" other members, not just a hiring role but shared ownership upfront
   * Relationship with a non-profit fiscal sponsor can be tricky - benefits, but accountable to members is different from non-profit accountability to public, retaining autonomy.



